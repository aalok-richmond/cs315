#!/bin/env python3
# Aalok S.
# CS 315: Algorithms, Fall 2018
# University of Richmond

import collections

################################################################################
######## class Graph
################################################################################
# This class represents a directed graph using adjacency matrix representation
# Obtained from
# https://en.wikipedia.org/wiki/
#   Ford-Fulkerson_algorithm#Python_implementation_of_Edmonds-Karp_algorithm
class Graph:
  
    def __init__(self,graph):
        self.graph = graph  # residual graph
        self.n_vertices = len(graph)
        # DEBUG
        # print("n_vertices:", self.n_vertices) 
        # print(graph)
  
    def BFS(self, s, t, parent):
        '''Returns true if there is a path from source 's' to sink 't' in
        residual graph. Also fills parent[] to store the path '''

        # Mark all the vertices as not visited
        visited = [False] * (self.n_vertices)
         
        # Create a queue for BFS
        queue = collections.deque()
         
        # Mark the source node as visited and enqueue it
        queue.append(s)
        visited[s] = True
         
        # Standard BFS Loop
        while queue:
            u = queue.popleft()

            # DEBUG
            # print("u, self.graph[u]", u, self.graph[u])         
            # Get all adjacent vertices's of the dequeued vertex u
            # If a adjacent has not been visited, then mark it
            # visited and enqueue it
            for key in self.graph[u]:
                if (visited[key] == False) and (self.graph[u][key] > 0):
                    queue.append(key)
                    visited[key] = True
                    parent[key] = u
 
        # If we reached sink in BFS starting from source, then return
        # true, else false
        return visited[t]
                     
    def DFS(self, s, t, parent, visited=None):
        '''Returns true if there is a path from source 's' to sink 't' in
        residual graph. Also fills parent[] to store the path '''

        if visited == None:
            visited = [False]*(self.n_vertices)

        if not visited[s]:
            visited[s] = True

        if s == t:
            return visited[s]
        
        # Standard DFS Loop
        for key in self.graph[s]:
            if (not visited[key]) and self.graph[s][key] > 0:
                parent[key] = s
                self.DFS(key, t, parent, visited)
                
        return visited[t]
             
    # Returns the maximum flow from s to t in the given graph
    def EdmondsKarp(self, source, sink):
 
        # This array is filled by BFS and to store path
        parent = [source] * (self.n_vertices)
 
        max_flow = 0 # There is no flow initially
 
        # Augment the flow while there is path from source to sink
        while self.BFS(source, sink, parent):
 
            # Find minimum residual capacity of the edges along the
            # path filled by BFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("inf")
            t = sink
            while t != source:
                path_flow = min(path_flow, self.graph[parent[t]][t])
                t = parent[t]
 
            # Add path flow to overall flow
            max_flow += path_flow
 
            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while v !=  source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]
 
        return max_flow

    # Returns the maximum flow from s to t in the given graph
    def FordFulkerson(self, source, sink):
 
        # This array is filled by DFS and to store path
        parent = [source] * (self.n_vertices)
 
        max_flow = 0 # There is no flow initially
 
        # Augment the flow while there is path from source to sink
        while self.DFS(source, sink, parent):
 
            # Find minimum residual capacity of the edges along the
            # path filled by DFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("inf")
            t = sink
            while t != source:
                path_flow = min(path_flow, self.graph[parent[t]][t])
                t = parent[t]
 
            # Add path flow to overall flow
            max_flow += path_flow
 
            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while v !=  source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]
 
        return max_flow

################################################################################
# end class Graph
################################################################################


while True:
    try:
        # n:children, m:toys, p:categories
        # read in input
        n,m,p = (int(x) for x in input().split())

        all_toys = set() # to keep track of all the toys

        child_toys = {} # what toys a child wants to play with
        for i in range(n): # n: no. of children
            child_toys[i] = set([int(i) for i in input().split()[1:]])
            all_toys.update(child_toys[i])

        categorized_toys = set() # to keep track of which toys have a category
        category_toys = {} # lists toys in each category
        max_category = [None] # max toys of a category that can be played
        for i in range(1,p+1): # p: no. of categories
            line = [int(i) for i in input().split()[1:]]
            category_toys[i] = set(line[:-1])
            categorized_toys.update(category_toys[i])
            all_toys.update(category_toys[i])
            max_category.append(int(line[-1]))
            
        category_toys[p+1] = all_toys.difference(categorized_toys)
        max_category.append(len(category_toys[p+1]))

        # 0 <-- toys --> 100 <-- children --> 200 <-- 300 (categories) 300 --> 400
        vertices = [300-i for i in range(1,p+2)] + [300+i for i in range(1,p+2)] \
                   + [*range(1+100,n+1+100)] + [*range(1,m+1)] \
                   + [-1, -2] # source and sink
        vertices = {i:vertices[i] for i in range(len(vertices))}

        # a reverse-lookup dictionary to get the index given a vertex name
        lookup = {vertices[i]:i for i in range(len(vertices))}
        # DEBUG
        # print(lookup.values(), len(lookup))

        # to store sparse adjacency of vertices
        adj_dict = {}

        # construct edges with appropriate weights. edge i,j is directed from i to j.
        adj_dict[lookup[-1]] = adj_dict.get(lookup[-1],{})
        adj_dict[lookup[-2]] = adj_dict.get(lookup[-2],{})
        
        # connect children
        for i in range(n):
            adj_dict[lookup[101+i]] = adj_dict.get(lookup[101+i],{})
            adj_dict[lookup[-1]][lookup[101+i]] = 1 # forward edge: 1 toy per child
            adj_dict[lookup[101+i]][lookup[-1]] = 0 # residual edge
            # connect children to toys
            for toy in child_toys[i]:
                adj_dict[lookup[toy]] = adj_dict.get(lookup[toy],{})
                adj_dict[lookup[101+i]][lookup[toy]] = 1 # forward edge: child likes toy
                adj_dict[lookup[toy]][lookup[101+i]] = 0 # residual edge
            
        # connect toys to categories
        for cat in range (1,p+2):
            adj_dict[lookup[300+cat]] = adj_dict.get(lookup[300+cat],{}) # in
            adj_dict[lookup[300-cat]] = {lookup[-2]:max_category[cat]} # out
            adj_dict[lookup[-2]][lookup[300-cat]] = 0 # out
            adj_dict[lookup[300+cat]][lookup[300-cat]] = max_category[cat] # vertex cap
            adj_dict[lookup[300-cat]][lookup[300+cat]] = 0 # residual vertex cap
            for toy in category_toys[cat]:
                adj_dict[lookup[toy]][lookup[300+cat]] = 1 # forward: toy of that category
                adj_dict[lookup[300+cat]][lookup[toy]] = 0 # residual
        
        # TODO DEBUG: print adjacency matrix
        # convert the sparse dictionary to a full matrix
        if 1:
            adj_matrix = [[adj_dict.get(i,{}).get(j,0) for j in range(len(vertices))]
                          for i in range(len(vertices))]
            _ = [print(vertices[index], '\t\t', adj_matrix[index])
                 for index in range(len(adj_matrix))]

        # construct a Graph object the graph class
        g = Graph(graph=adj_dict)

        # run Ford-Fulkerson on g
        maxflow = g.FordFulkerson(source=lookup[-1], sink=lookup[-2])

        print(maxflow)

    except Exception:
        break
