#!/bin/env python3
# Aalok S.
# CS 315: Algorithms, Fall 2018
# University of Richmond

import collections

################################################################################
######## class Graph
################################################################################
# This class represents a directed graph using adjacency matrix representation
# Obtained from
# https://en.wikipedia.org/wiki/
#   Ford-Fulkerson_algorithm#Python_implementation_of_Edmonds-Karp_algorithm
class Graph:
  
    def __init__(self,graph):
        self.graph = graph  # residual graph
        self.n_vertices = len(graph)
        # DEBUG
        # print("n_vertices:", self.n_vertices) 
        # print(graph)
  
    def BFS(self, s, t, parent):
        '''Returns true if there is a path from source 's' to sink 't' in
        residual graph. Also fills parent[] to store the path '''

        # Mark all the vertices as not visited
        visited = [False] * (self.n_vertices)
         
        # Create a queue for BFS
        queue = collections.deque()
         
        # Mark the source node as visited and enqueue it
        queue.append(s)
        visited[s] = True
         
        # Standard BFS Loop
        while queue:
            u = queue.popleft()

            # DEBUG
            # print("u, self.graph[u]", u, self.graph[u])         
            # Get all adjacent vertices's of the dequeued vertex u
            # If a adjacent has not been visited, then mark it
            # visited and enqueue it
            for key in self.graph[u]:
                if (visited[key] == False) and (self.graph[u][key] > 0):
                    queue.append(key)
                    visited[key] = True
                    parent[key] = u
 
        # If we reached sink in BFS starting from source, then return
        # true, else false
        return visited[t]
                     
    def DFS(self, s, t, parent, visited=None):
        '''Returns true if there is a path from source 's' to sink 't' in
        residual graph. Also fills parent[] to store the path '''

        if visited == None:
            visited = [False]*(self.n_vertices)

        if not visited[s]:
            visited[s] = True

        if s == t:
            return visited[s]
        
        # Standard DFS Loop
        for key in self.graph[s]:
            if (not visited[key]) and self.graph[s][key] > 0:
                parent[key] = s
                self.DFS(key, t, parent, visited)
                
        return visited[t]
             
    # Returns the maximum flow from s to t in the given graph
    def EdmondsKarp(self, source, sink):
 
        # This array is filled by BFS and to store path
        parent = [source] * (self.n_vertices)
 
        max_flow = 0 # There is no flow initially
 
        # Augment the flow while there is path from source to sink
        while self.BFS(source, sink, parent):
 
            # Find minimum residual capacity of the edges along the
            # path filled by BFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("inf")
            t = sink
            while t != source:
                path_flow = min(path_flow, self.graph[parent[t]][t])
                t = parent[t]
 
            # Add path flow to overall flow
            max_flow += path_flow
 
            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while v !=  source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]
 
        return max_flow

    # Returns the maximum flow from s to t in the given graph
    def FordFulkerson(self, source, sink):
 
        # This array is filled by DFS and to store path
        parent = [source] * (self.n_vertices)
 
        max_flow = 0 # There is no flow initially
 
        # Augment the flow while there is path from source to sink
        while self.DFS(source, sink, parent):
 
            # Find minimum residual capacity of the edges along the
            # path filled by DFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("inf")
            t = sink
            while t != source:
                path_flow = min(path_flow, self.graph[parent[t]][t])
                t = parent[t]
 
            # Add path flow to overall flow
            max_flow += path_flow
 
            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while v !=  source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]
 
        return max_flow

################################################################################
# end class Graph
################################################################################

#sample_input = """5
#9 22 21 15 13
#0 5 4 4 2
#5 0 5 4 3
#4 5 0 2 5
#4 4 2 0 7
#2 3 5 7 0""".split('\n')

while True:
    try:
        # number of states, raw material sites, factories, and transportation companies
        # read in input
        s,r,f,t = [int(x) for x in input().split()]

        raw_mat_states = set(input().split())
        factory_states = set(input().split())
        other_states = set([])
        
        transport_co = []
        for i in range(t+1):
            if not i:
                transport_co.append(None)
                continue
                
            served = input().split()
            raw = [x for x in served if x in raw_mat_states]
            fac = [x for x in served if x in factory_states]
            other = [x for x in served[1:]
                     if (x not in raw_mat_states) and (x not in factory_states)]
            other_states.update(other)
            transport_co.append([raw,fac,other])

        vertices = [2000+i for i in range(1,t+1)] + [2000-i for i in range(1,t+1)] \
                   + [*raw_mat_states] + [*factory_states] + [*other_states] \
                   + [-1, -2] # source and sink
        vertices = {i:vertices[i] for i in range(len(vertices))}

        # a reverse-lookup dictionary to get the index given a vertex name
        lookup = {vertices[i]:i for i in range(len(vertices))}
        # DEBUG
        # print(lookup.values(), len(lookup))

        # to store sparse adjacency of vertices
        adj_dict = {}

        # construct edges with appropriate weights. edge i,j is directed from i to j.
        adj_dict[lookup[-1]] = adj_dict.get(lookup[-1],{})
        adj_dict[lookup[-2]] = adj_dict.get(lookup[-2],{})
        
        # connect source to raw material states
        for i in raw_mat_states:
            adj_dict[lookup[i]] = adj_dict.get(lookup[i],{})
            adj_dict[lookup[-1]][lookup[i]] = 1 # forward edge
            adj_dict[lookup[i]][lookup[-1]] = 0 # residual edge
            
        # connect factories to sink
        for i in factory_states:
            adj_dict[lookup[i]] = adj_dict.get(lookup[i],{})
            adj_dict[lookup[i]][lookup[-2]] = 1 # forward edge
            adj_dict[lookup[-2]][lookup[i]] = 0 # residual edge
            
        # make dummy nodes for `other' states (non-raw mat., non-factory)
        for i in other_states:
            adj_dict[lookup[i]] = adj_dict.get(lookup[i],{})
            
        # connect each transport company to raw mat./factory states
        for i in range(1, t+1):
            adj_dict[lookup[2000+i]] = adj_dict.get(lookup[2000+i],{})
            adj_dict[lookup[2000-i]] = adj_dict.get(lookup[2000-i],{})
            adj_dict[lookup[2000+i]][lookup[2000-i]] = 1 # forward edge
            adj_dict[lookup[2000-i]][lookup[2000+i]] = 0 # residual edge
            
            # connect raw materials to transport companies
            for raw in transport_co[i][0]:
                adj_dict[lookup[raw]][lookup[2000+i]] = 1 # forward edge
                adj_dict[lookup[2000+i]][lookup[raw]] = 0 # residual edge
                
            # connect transport companies to factories
            for fac in transport_co[i][1]:
                adj_dict[lookup[2000-i]][lookup[fac]] = 1 # forward edge
                adj_dict[lookup[fac]][lookup[2000-i]] = 0 # residual edge
                
            # connect transport companies to other states and back
            for oth in transport_co[i][2]:
                # disjoint T_in edges
                adj_dict[lookup[oth]][lookup[2000+i]] = 1 # forward
                adj_dict[lookup[2000+i]][lookup[oth]] = 0 # residual
                # disjoint T_out edges
                adj_dict[lookup[2000-i]][lookup[oth]] = 1 # forward
                adj_dict[lookup[oth]][lookup[2000-i]] = 0 # residual
            
        # TODO DEBUG: print adjacency matrix
        # convert the sparse dictionary to a full matrix
        # adj_matrix = [[adj_dict.get(i,{}).get(j,0) for j in range(len(vertices))]
        #              for i in range(len(vertices))]
        #_ = [print(vertices[index], '\t\t', adj_matrix[index])
        #     for index in range(len(adj_matrix))]

        # construct a Graph object the graph class
        g = Graph(graph=adj_dict)

        # run Ford-Fulkerson on g
        maxflow = g.FordFulkerson(source=lookup[-1], sink=lookup[-2])

        print(maxflow)

    except Exception:
        break
