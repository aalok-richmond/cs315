#!/bin/env python3
# Aalok S.
# CS 315: Algorithms, Fall 2018
# University of Richmond

import copy
import collections

################################################################################
######## class Graph
################################################################################
# This class represents a directed graph using adjacency matrix representation
# Obtained from
# https://en.wikipedia.org/wiki/
#   Ford-Fulkerson_algorithm#Python_implementation_of_Edmonds-Karp_algorithm
class Graph:
  
    def __init__(self,graph):
        self.graph = graph  # residual graph
        self.ROW = len(graph)
  
    def BFS(self, s, t, parent):
        '''Returns true if there is a path from source 's' to sink 't' in
        residual graph. Also fills parent[] to store the path '''

        # Mark all the vertices as not visited
        visited = [False] * (self.ROW)
         
        # Create a queue for BFS
        queue = collections.deque()
         
        # Mark the source node as visited and enqueue it
        queue.append(s)
        visited[s] = True
         
        # Standard BFS Loop
        while queue:
            u = queue.popleft()
         
            # Get all adjacent vertices's of the dequeued vertex u
            # If a adjacent has not been visited, then mark it
            # visited and enqueue it
            for ind, val in enumerate(self.graph[u]):
                if (visited[ind] == False) and (val > 0):
                    queue.append(ind)
                    visited[ind] = True
                    parent[ind] = u
 
        # If we reached sink in BFS starting from source, then return
        # true, else false
        return visited[t]
             
    # Returns the maximum flow from s to t in the given graph
    def EdmondsKarp(self, source, sink):
 
        # This array is filled by BFS and to store path
        parent = [-1] * (self.ROW)
 
        max_flow = 0 # There is no flow initially
 
        # Augment the flow while there is path from source to sink
        while self.BFS(source, sink, parent):
 
            # Find minimum residual capacity of the edges along the
            # path filled by BFS. Or we can say find the maximum flow
            # through the path found.
            path_flow = float("Inf")
            s = sink
            while s != source:
                path_flow = min(path_flow, self.graph[parent[s]][s])
                s = parent[s]
 
            # Add path flow to overall flow
            max_flow += path_flow
 
            # update residual capacities of the edges and reverse edges
            # along the path
            v = sink
            while v !=  source:
                u = parent[v]
                self.graph[u][v] -= path_flow
                self.graph[v][u] += path_flow
                v = parent[v]
 
        return max_flow

################################################################################
# end class Graph
################################################################################

#sample_input = """5
#9 22 21 15 13
#0 5 4 4 2
#5 0 5 4 3
#4 5 0 2 5
#4 4 2 0 7
#2 3 5 7 0""".split('\n')

# total number of teams, including the team of interest
n_teams = int(input())

# how many wins each team has (supplied as an input)
wins = [int(i) for i in input().split()] # list to store wins_of(v) at index v

# how many games do i,j have remaining with each other
remaining = [[int(r) for r in input().split()] for i in range(n_teams)]

# we construct a vertex for
#   - each team (but we don't use the vertex for 0)
#   - each matching i,j for all teams but 0
#   - source s and sink t
vertices = [i for i in range(n_teams)] \
           + [(i,j) for i in range(n_teams) for j in range(n_teams)] \
           + ['s','t']
vertices = {i:vertices[i] for i in range(len(vertices))}

# a reverse-lookup dictionary to get the index given a vertex name
lookup = {vertices[i]:i for i in range(len(vertices))}

# to store sparse adjacency of vertices
adj_dict = {0:{0:0}}

# construct edges with appropriate weights. edge i,j is directed from i to j.
adj_dict[lookup['s']] = adj_dict.get(lookup['s'],{lookup['s']:0})
adj_dict[lookup['t']] = adj_dict.get(lookup['t'],{lookup['t']:0})
for i in range(1, n_teams):
    adj_dict[lookup[i]] = adj_dict.get(lookup[i],{i:0})
    for j in range(i+1, n_teams):
        adj_dict[lookup[(i,j)]] = adj_dict.get(lookup[(i,j)],{})
        adj_dict[lookup['s']][lookup[(i,j)]] = remaining[i][j]
        adj_dict[lookup[(i,j)]][lookup[i]] = remaining[i][j]
        adj_dict[lookup[(i,j)]][lookup[j]] = remaining[i][j]
    adj_dict[lookup[i]][lookup['t']] = wins[0] + sum(remaining[0]) - wins[i]

# convert the sparse dictionary to a full matrix
adj_matrix = [[adj_dict.get(i,{}).get(j,0) for j in range(len(vertices))]
              for i in range(len(vertices))]

# DEBUG: print adjacency matrix
#_ = [print(vertices[index], '\t\t', adj_matrix[index])
#     for index in range(len(adj_matrix))]

# construct a Graph object the graph class. pass in a non-shallow-copied matrix
g = Graph(graph=copy.deepcopy(adj_matrix))

# run Edmonds-Karp on g
maxflow = g.EdmondsKarp(source=lookup['s'], sink=lookup['t'])

# find ideal flow if all games playable (total capacity in cut s-{i,j})
maxcap = 0
for i in range(1,n_teams):
    for j in range(i+1,n_teams):
        maxcap += adj_dict.get(lookup['s'],{}).get(lookup[(i,j)],0)

# compare ideal flow to actual flow and make a decision: are they equal?
if maxcap==maxflow:
    print("yes")
else:
    print("no")

# DEBUG:
#print("total games playable:", maxcap, "\ttotal played:",
#        maxflow, "\n\nteam 0 tie/win possible?", maxcap==maxflow)
